# 2024 Q3

The purpose of the following document is to summarize the major changes to the server and their impact on the server growth and health.

## Key Highlights
<!-- List major highlights below -->
<!-- You can use the milestones in the Staff Documentation repo to more accurately see what was changed -->
- `general-media` was turned into a Forum-style channel ([#144](https://gitlab.com/the-mermaids-rock/staff-documentation/-/issues/144)).
- Discord User Apps trialed for Shellbacks ([#142](https://gitlab.com/the-mermaids-rock/staff-documentation/-/issues/142)).
- Warning system was [documented](https://www.the-mermaids-rock.nl/general/warning-system/) ([#2](https://gitlab.com/the-mermaids-rock/public/-/issues/2)).
- Minecraft Server was updated to `1.21.1` ([#5](https://gitlab.com/the-mermaids-rock/minecraft-server-operations/-/issues/5)).
- Edinburgh now automatically removes mothballed users after 30 days ([#47](https://gitlab.com/the-mermaids-rock/development/edinburgh/-/issues/47)).
- Edinburgh got a `/tinchicken` command to more easily call in the `Tin Chicken` rule.

## Server Health
<!-- Update the following section to reflect the changes in membership and messages sent during this period -->
This quarter ended with 367 members, which is 18 higher than the previous quarter at 349.  
During this period, a total of 148794 messages were send in the server, this does not include the messages sent by bots.

## Changes in Staff
<!-- List the changes in staff membership -->
There was no change in staff membership during this period.

## Detailed Report
Below we will write the key observations for this quarter.  
Please do note that, while we try to base this conclusion on observations made by the staff, they remain a subjective topic.  
This section may not accurately reflect reality and is provided "as-is" despite our best efforts.

### Minecraft Server

The Minecraft server was finally updated to 1.21.1 after a long time of being backlogged, however, we haven't seen it cause a noteworthy resurgence in the playerbase.  
As such, we might need to, again, evaluate the viability of keeping the Minecraft server up and running 24/7 due to the resource waste.  

### Tin Chicken

We have seen some invocations of the Tin Chicken during this quarter.  
However, some people didn't understand what it was and as such, the `/tinchicken` command was added to Edinburgh which gives an explanation of what it does, as well as make it easier for Staff to get informed of it being called.  
Some Patrons still do keep going after the Tin Chicken was called in and we may need to be harder on the punishments in these cases.

### Mothball Purge

We have added a new mechanism that allows Edinburgh to automatically purge mothballs that did not successfully appeal in 30 days.  
This would allow us to shrink down the member count on Disboard a bit in the hopes that people looking for a smaller server may be more likely to join, as well as just being closer to the "real number of Patrons".

There has been a small showcase of this already on the 9th of September, however, due to a bug, a lot of the mothballs before a certain date were removed from the database and as such, their purge was delayed until the 9th of October (2024Q4).  
As such, we still have to see the effectiveness of this system.

## Final Words
<!-- Some final words by First Host -->
This quarter was a pretty laid-back one overall, chat was fairly calm with some spikes in activity here and there.  
New developments on the server have been going slow because the most important things have currently already been done.

A big thing we've noticed was where new member influx has dipped below 20.  
This may be in part due to Disboard bumps seemingly becoming more unstable, as well as the ever increasing amount of competitors on Disboard.  
This isn't a direct cause of concern, as we value the quality of our Patrons over the quantity.
However, we'll need to find more ways to attract Patrons in the future.

All in all, I'd say that things were fine this quarter, but could have been significantly better.  
We do hope that 2024Q4 will be as successful as [2023Q4](https://gitlab.com/the-mermaids-rock/quarterly-reports/-/blob/main/reports/2023%20Q3.md?ref_type=heads), however, we'll have to see what the future holds.

<!-- End of report -->
**End of report**  
**Signed off by First Host: FinlayDaG33k**
