# 20YY QN

The purpose of the following document is to summarize the major changes to the server and their impact on the server growth and health.

## Key Highlights
<!-- List major highlights below -->
<!-- You can use the milestones in the Staff Documentation repo to more accurately see what was changed -->


[AWAITING END OF QUARTER]

## Server Health
<!-- Update the following section to reflect the changes in membership and messages sent during this period -->
This quarter ended with ??? members, which is ?? higher/lower than the previous quarter at ???.  
During this period, a total of ??? messages were send in the server, this does not include the messages sent by bots.

[AWAITING END OF QUARTER]

## Changes in Staff
<!-- List the changes in staff membership -->
There was no change in staff membership during this period.

[AWAITING END OF QUARTER]

## Detailed Report
Below we will write the key observations for this quarter.  
Please do note that, while we try to base this conclusion on observations made by the staff, they remain a subjective topic.  
This section may not accurately reflect reality and is provided "as-is" despite our best efforts.


## Final Words
<!-- Some final words by First Host -->

[AWAITING END OF QUARTER]

<!-- End of report -->
**End of report**  
**Signed off by First Host: FinlayDaG33k**
